# Containers

Experiments with containers for building C++ projects or just minimal docker images for certain purposes.

# Dumping ground for building static executables

gcc -Wl,--build-id=none ../minimal.c -c -o test.o
gcc -Wl,--build-id=none ../minimal.c -fpie -c -o testpie.o
gcc -Wl,--build-id=none ../minimal.c -fpic -c -o testpic.o

gcc -Wl,--build-id=none test.o -o test-dynamic
gcc -Wl,--build-id=none testpie.o -o test-dynamic-pie
gcc -Wl,--build-id=none testpic.o -o test-dynamic-pic

gcc -Wl,--build-id=none test.o --static -o test-static
gcc -Wl,--build-id=none testpie.o --static -o test-pie-static
gcc -Wl,--build-id=none testpic.o --static -o test-pic-static

gcc -Wl,--build-id=none test.o --static-pie -o test-static-pie
gcc -Wl,--build-id=none testpie.o --static-pie -o test-pie-static-pie
gcc -Wl,--build-id=none testpic.o --static-pie -o test-pic-static-pie
