FROM docker.io/library/debian:testing

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install cmake ninja-build --no-install-recommends -y
RUN apt-get install clang-13 lld-13 --no-install-recommends -y
ENV PATH="/usr/lib/llvm-13/bin:${PATH}"
RUN apt-get install libfmt-dev --no-install-recommends -y
