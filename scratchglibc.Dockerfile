FROM docker.io/library/debian:testing

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install gcc-11 --no-install-recommends -y
RUN apt-get install make gawk bison python3 --no-install-recommends -y
RUN apt-get install ca-certificates wget xz-utils --no-install-recommends -y
RUN wget https://ftp.gnu.org/gnu/glibc/glibc-2.34.tar.xz
RUN tar -xf glibc-2.34.tar.xz
WORKDIR glibc-2.34/build
RUN CC=gcc-11 ../configure --prefix=/i
RUN make -j32
RUN make install


FROM scratch
COPY --from=0 /i ./
